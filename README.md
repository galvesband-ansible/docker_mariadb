Role Name
=========

A simple Ansible role to deploy MariaDB on Docker in Ubuntu 20.04.

Requirements
------------

The role expects the host to have Docker correctly configured and running, and access to it. Specifically, 
it is meant to work in conjuntion with [this docker role](https://gitlab.com/galvesband-ansible/docker).

Role Variables
--------------

 - `mariadb_version`: Container version to use. By default: `10.6-focal`.

 - `mariadb_network`: Docker network the container should be in. By default: `default`.

 - `mariadb_exposed_port`: Port where MariaDB will be exposed on the host, in `127.0.0.1`. By default: `3306`.

 - `mariadb_root_password`: Root user password. Mandatory (no default). Don't use plain text passwords, use Vault or something!

 - `mariadb_databases`: List of databases to ensure they exists. Empty by default.

 - `mariadb_users`: List of dictionaries where each element looks like this:
   - `name`: User name.
     `password`: User password. Don't use plain text passwords, use Vault or something!
     `privileges_on`: Dictionary with databases -dot- tables as keys and privileges as values. For example: `some_other_database.*: "ALL"`.

Dependencies
------------

The role by itself doesn't require any other role. It uses docker and mysql collections from `community`. There should be a 
Docker installation in the system. Look at [this docker role](https://gitlab.com/galvesband-ansible/docker), for example.

Example Playbook
----------------

```yml
---
- name: MariaDb
  hosts: ubuntu2004
  tasks:

    - name: "Install docker"
      include_role:
        name: "docker"

    - name: "Include mariadb"
      include_role:
        name: "docker_mariadb"
        
      vars:
        mariadb_network: my-network
        mariadb_root_password: supersecret
        mariadb_databases:
          - somedb
          - someotherdb
        mariadb_users:
          - name: myuser
            password: lerele
            privileges_on:
              someotherdb.*: "ALL"

```

License
-------

GLP 2.0 or later.
